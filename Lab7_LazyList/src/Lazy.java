import java.lang.NullPointerException;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.function.Function;
import java.util.function.BiFunction;
import java.util.function.Predicate;

class Lazy<T extends Comparable<T>> {
    private final Supplier<? extends T> supplier;
    private Optional<T> cache;

    private Lazy(Supplier<? extends T> supplier) {
        this.supplier = Optional.ofNullable(supplier).orElseThrow(() -> { return new NoSuchElementException("No value present"); });
        this.cache = Optional.empty();
    }

    public static <U extends Comparable<U>> Lazy<U> of(U value) {
       Supplier<U> supplier = () -> { return value; };
       return new Lazy<U>(supplier);
    }

    public static <U extends Comparable<U>> Lazy<U> of(Supplier<? extends U> supplier) {
        return new Lazy<U>(supplier);
    }

    @Override
    public String toString() {
        Function<T, String> strConvert = (x) -> { return x.toString(); };
        Optional<String> strOp = cache.map(strConvert);
        return strOp.orElse("?");
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Lazy) {
            Lazy<?> lazy = (Lazy<?>)obj;
            return this.get().equals(lazy.get());
        }
        return false;
    }

    public T get() {
        T evaluated = cache.orElseGet(supplier);
        cache = Optional.of(evaluated);
        return evaluated;
    }

    public <U extends Comparable<U>> Lazy<U> map(Function<? super T, ? extends U> mapper) {
        Supplier<U> supplier = () -> { return mapper.apply(this.get()); };
        return new Lazy<U>(supplier);
    }

    public <U extends Comparable<U>> Lazy<U> flatMap(Function<? super T, ? extends Lazy<? extends U>> mapper) {
        Supplier<U> supplier = () -> { return mapper.apply(this.get()).get(); };
        return new Lazy<U>(supplier);
    }

    public <U extends Comparable<U>, W extends Comparable<W>> Lazy<W> combine(Lazy<? extends U> lazy, BiFunction<? super T, ? super U, ? extends W> combinator) {
        Supplier<W> supplier = () -> { return combinator.apply(this.get(), lazy.get()); };
        return new Lazy<W>(supplier);
    }

    public Lazy<Boolean> test(Predicate<? super T> predicate) {
        Supplier<Boolean> supplier = () -> { return predicate.test(this.get()); };
        return new Lazy<Boolean>(supplier);
    }

    public Lazy<Integer> compareTo(Lazy<? extends T> lazy) {
        Supplier<Integer> supplier = () -> { return this.get().compareTo(lazy.get()); };
        return new Lazy<Integer>(supplier);
    }
}
