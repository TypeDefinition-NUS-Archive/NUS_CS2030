Function<Room, Room> takeSword = (Room room) -> {
    if (room.inInventory("Sword")) {
        System.out.println("--> You already have sword.");
        return room;
    }

    if (!room.inRoom("Sword")) {
        System.out.println("--> There is no sword.");
        return room;
    }

    System.out.println("--> You have taken sword.");
    return room.take("Sword");
}

Function<Room, Room> dropSword = (Room room) -> {
    if (!room.inInventory("Sword")) {
        System.out.println("--> You have no sword.");
        return room;
    }

    System.out.println("--> You have dropped sword.");
    return room.drop("Sword");
}

Function<Room, Room> killTroll = (Room room) -> {
    if (!room.inRoom("Troll")) {
        System.out.println("--> There is no troll");
        return room;
    }

    if (!room.inInventory("Sword")) {
        System.out.println("--> You have no sword.");
        return room;
    }

    System.out.println("--> Troll is killed.");
    return room.remove("Troll");
}
