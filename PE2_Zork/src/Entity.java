public abstract class Entity {
    private final String name;

    public Entity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public final boolean nameIs(String name) {
        return this.name.equals(name);
    }

    public abstract Entity tick();
}
