import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.Stream;
import java.util.stream.IntStream;
import java.util.stream.Collectors;
import java.util.function.Function;
import java.util.function.Consumer;
import java.util.function.IntConsumer;
import java.util.function.Predicate;

public class Room {
    private final String name;
    private final List<Entity> entities;
    private final List<Entity> inventory;
    private final Optional<Room> back;
    
    private Room(String name, List<Entity> entities, List<Entity> inventory, Optional<Room> back) {
        this.name = name;
        this.entities = entities;
        this.inventory = inventory;
        this.back = back;
    }

    private Room(String name, List<Entity> entities, List<Entity> inventory) {
        this(name, entities, inventory, Optional.empty());
    }

    public Room(String name, List<Entity> entities) {
        this(name, entities, new ArrayList<Entity>());
    }

    public Room(String name) {
        this(name, new ArrayList<Entity>());
    }

    @Override
    public String toString() {
        String roomDesc = String.format("@%s\n", name);
        String entityDesc = entities.stream().
                map(Object::toString).
                collect(Collectors.joining("\n"));
        return roomDesc.concat(entityDesc);
    }

    public Room add(Entity entity) {
        if (inRoom(entity.getName())) {
            return this;
        }
        
        List<Entity> newEntities = new ArrayList<Entity>(entities);
        newEntities.add(entity);
        return new Room(name, newEntities, inventory, back);
    }

    public Room remove(String entityName) {
        // Check if an entity has a different name.
        Predicate<Entity> notSameName = (e) -> {
            return !e.nameIs(entityName);
        };

        // Remove entities that has the same name as entityName.
        List<Entity> newEntities = entities.stream().
                filter(notSameName).
                collect(Collectors.toList());

        // Remove entities that has the same name as entityName.
        List<Entity> newInventory = inventory.stream().
                filter(notSameName).
                collect(Collectors.toList());

        return new Room(name, newEntities, newInventory, back);
    }

    public Room replace(Entity entity) {
        // If an entity in the list has the same name, replace it.
        Function<Entity, Entity> replaceSameName = (e) -> {
            return e.nameIs(entity.getName()) ? entity : e;
        };

        // Replace entities with the same name as entity.
        List<Entity> newEntities = entities.stream().
                map(replaceSameName).
                collect(Collectors.toList());

        // Replace entities with the same name as entity.
        List<Entity> newInventory = inventory.stream().
                map(replaceSameName).
                collect(Collectors.toList());

        return new Room(name, newEntities, newInventory, back);
    }

    public boolean inRoom(String entityName) {
        return entities.stream().anyMatch((e) -> e.nameIs(entityName));
    } 

    public boolean inInventory(String entityName) {
        return inventory.stream().anyMatch((e) -> e.nameIs(entityName));
    }

    public Room take(String entityName) {
        if (inInventory(entityName)) {
            return this;
        }

        // Check if an entity has a different name.
        Predicate<Entity> sameName = (e) -> {
            return e.nameIs(entityName);
        };

        List<Entity> newInventory = Stream.
                concat(inventory.stream(), entities.stream().filter(sameName)).
                collect(Collectors.toList());

        return new Room(name, entities, newInventory, back);
    }

    public Room drop(String entityName) {
        // Check if an entity has a different name.
        Predicate<Entity> notSameName = (e) -> {
            return !e.nameIs(entityName);
        };

        // Remove entities that has the same name as entityName.
        List<Entity> newInventory = inventory.stream().
                filter(notSameName).
                collect(Collectors.toList());

        return new Room(name, entities, newInventory, back);
    }

    public Room tick() {
        List<Entity> newEntities = entities.stream().
                map((e) -> e.tick()).
                collect(Collectors.toList());
        return new Room(name, newEntities, inventory, back);
    }

    public Room tick(Function<Room, Room> functor) {
        return functor.apply(this).tick();
    }

    public Room go(Function<List<Entity>, Room> functor) {
        Predicate<Entity> notInInventory = (e) -> {
            return !this.inInventory(e.getName());
        };
        List<Entity> thisRoomEntities = entities.stream().
                filter(notInInventory).
                collect(Collectors.toList());

        // Create a snapshot of the current room to go back to later.
        Room thisRoom = new Room(name, thisRoomEntities, new ArrayList<Entity>(), back);

        Room nextRoom = functor.apply(thisRoomEntities);
        List<Entity> nextRoomEntities = new ArrayList<Entity>(inventory);
        nextRoomEntities.addAll(nextRoom.entities);

        List<Entity> nextRoomInventory = new ArrayList<Entity>(inventory);
        nextRoomInventory.addAll(nextRoom.inventory);

        // Alrighty boys, let's go.
        return new Room(nextRoom.name, nextRoomEntities, nextRoomInventory, Optional.of(thisRoom));
    }

    public Room back() {
        Room previousRoom = back.orElse(this);
        if (previousRoom == this) {
            return this;
        }

        List<Entity> previousRoomEntities = new ArrayList<Entity>(previousRoom.entities);
        previousRoomEntities.addAll(inventory);

        return new Room(previousRoom.name, previousRoomEntities, inventory, previousRoom.back).tick();
    }
}
