public class Sword extends Entity {
    private enum State {
        SHIMMER("Sword is shimmering.");

        private final String description;

        private State(String description) {
            this.description = description;
        }

        public String getDescription() {
            return this.description;
        }

        public State nextState() {
            int nextOrdinal = Math.min(values().length - 1, ordinal() + 1);
            return values()[nextOrdinal];
        }
    }
    
    private final State state;

    public Sword(State state) {
        super("Sword");
        this.state = state;
    }

    public Sword() {
        this(State.SHIMMER);
    }

    @Override
    public String toString() {
        return state.getDescription();
    }

    @Override
    public Sword tick() {
        return new Sword(state.nextState());
    }
}
