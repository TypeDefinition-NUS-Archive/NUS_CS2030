package cs2030.simulator;

import java.util.List;

/**
 * An event representing a Customer being served.
 */
public class ServeEvent extends SimulatorEvent {
    /**
     * identifier of the Server that is serving customer.
     */
    private final int serverIdentifier;
    /**
     * the time this event occurred.
     */
    private final double eventTime;

    /**
     * how long it takes to service a customer.
     */
    private final static double SERVICE_TIME = 1.0; // does not fit CS2030 style guide, but CodeCrunch will reject it if I declare it as private static final.

    /**
     * Constructs a new ServeEvent with a customer, server list and server identifier.
     * @param customer the customer that this event represents.
     * @param serverList the list of servers.
     * @param serverIdentifier The identifier of the server that the customer is served by.
     */
    public ServeEvent(Customer customer, List<Server> serverList, int serverIdentifier) {
        super(customer, serverList);
        this.serverIdentifier = serverIdentifier;
        this.eventTime = Math.max(getCustomer().getArrivalTime(),
                getServer(serverIdentifier).getNextAvailableTime());
    }

    /**
     * Returns a string representation of the object.
     * Shows the event time, customer ID and the Server the customer is being served by.
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return String.format("%.3f %d served by %d",
                getEventTime(), getCustomerID(), serverIdentifier);
    }

    @Override
    public double getEventTime() {
        return eventTime;
    }

    /**
     * Execute the event.
     * @return a DoneEvent.
     */
    @Override
    public SimulatorEvent execute() {
        replaceServer(getServer(serverIdentifier)
                .setIsAvailable(false)
                .setHasWaitingCustomer(false)
                .setNextAvailableTime(getEventTime() + SERVICE_TIME));                
        return new DoneEvent(getCustomer(), getServerList(), serverIdentifier);
    }
}