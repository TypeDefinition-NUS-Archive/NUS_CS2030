package cs2030.simulator;

import java.util.List;

/**
 * SimulatorEvent is the base class of the event classes.
 */
public abstract class SimulatorEvent {
    /**
     * the customer that this event represents.
     */
    private final Customer customer;

    /**
     * the list of servers.
     */
    private final List<Server> serverList;

    /**
     * Creates a new Simulator with a customer and server list.
     * Since Simulator is an abstract class, this constructor cannot be directly called.
     * @param customer the customer that this event represents.
     * @param serverList the list of servers.
     */
    public SimulatorEvent(Customer customer, List<Server> serverList) {
        this.customer = customer;
        this.serverList = serverList;
    }

    /**
     * Returns a server matching the identifier given.
     * @param identifier identifier of the server.
     * @return a server matching the identifier given.
     * @throws IllegalArgumentException if there are no servers matching the identifier.
     */
    protected Server getServer(int identifier) {
        for (Server server : serverList) {
            if (server.getIdentifier() == identifier) {
                return server;
            }
        }

        throw new IllegalArgumentException("No server matching the identifier found.");
    }

    /**
     * Searches the server list, and if there is a server with
     * the same identiier as the replacement server, it is replaced.
     * @param server the replacement server.
     * @throws IllegalArgumentException if there are no servers
     *        matching the identifier of the replacement server.
     */
    protected void replaceServer(Server server) {
        for (int i = 0; i < serverList.size(); ++i) {
            if (server.getIdentifier() == serverList.get(i).getIdentifier()) {
                serverList.set(i, server);
                return;
            }
        }

        throw new IllegalArgumentException("No server matching the replacement server found.");
    }

    /**
     * Get the customer of the event.
     * @return the customer of this event.
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Get the customer ID of the event.
     * @return the customer ID of this event.
     */
    public int getCustomerID() {
        return customer.getID();
    }

    /**
     * Get the customer arrival time of the event.
     * @return the customer arrival time of this event.
     */
    public double getCustomerArrivalTime() {
        return customer.getArrivalTime();
    }

    /**
     * Get the server list.
     * @return the server list.
     */
    public List<Server> getServerList() {
        return serverList;
    }

    /**
     * Get the time which the event occurred.
     * @return the time which the event occurred.
     */
    public abstract double getEventTime();

    /**
     * Execute the event.
     */
    public abstract SimulatorEvent execute();
}