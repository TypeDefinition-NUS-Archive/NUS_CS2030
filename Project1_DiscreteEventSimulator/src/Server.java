package cs2030.simulator;

/**
 * Represents a Server. A Server serves Customer that arrive.
 */
public class Server {
    /**
     * identifier of the Server. Similar to an ID.
     */
    private final int identifier;
    /**
     * flag to check if the Server is able to serve a Customer.
     */
    private final boolean isAvailable;
    /**
     * flag to check if the Server has a Customer waiting.
     */
    private final boolean hasWaitingCustomer;
    /**
     * If the Server is unavailable, this is
     * the time at which the Server will become available again.
     */
    private final double nextAvailableTime;

    /**
     * Creates a new Server with an identifier,
     * available flag, has waiting customer flag, and next available time.
     * @param identifier identifier of the Server. Similar to an ID.
     * @param isAvailable flag to check if the Server is able to serve a Customer.
     * @param hasWaitingCustomer flag to check if the Server has a Customer waiting.
     * @param nextAvailableTime If the Server is unavailable,
     *                          this is the time at which the Server will become available again.
     */
    public Server(int identifier, boolean isAvailable,
            boolean hasWaitingCustomer, double nextAvailableTime) {
        this.identifier = identifier;
        this.isAvailable = isAvailable;
        this.hasWaitingCustomer = hasWaitingCustomer;
        this.nextAvailableTime = nextAvailableTime;
    }

    /**
     * Returns a string representation of the object.
     * Shows the identifier status and waiting customer (if any).
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        if (isAvailable) {
            return String.format("%d is available", this.identifier);
        }

        return String.format("%d is busy; %s %.3f",
                this.identifier,
                this.hasWaitingCustomer ? "waiting customer to be served at" : "available at",
                this.nextAvailableTime);
    }

    public int getIdentifier() {
        return identifier;
    }

    public boolean getIsAvailable() {
        return isAvailable;
    }

    public boolean getHasWaitingCustomer() {
        return hasWaitingCustomer;
    }
   
    public double getNextAvailableTime() {
        return nextAvailableTime;
    }

    public Server setIsAvailable(boolean isAvailable) {
        return new Server(identifier, isAvailable, hasWaitingCustomer, nextAvailableTime);
    }

    public Server setHasWaitingCustomer(boolean hasWaitingCustomer) {
        return new Server(identifier, isAvailable, hasWaitingCustomer, nextAvailableTime);
    }

    public Server setNextAvailableTime(double nextAvailableTime) {
        return new Server(identifier, isAvailable, hasWaitingCustomer, nextAvailableTime);
    }
}