package cs2030.simulator;

import java.util.List;

/**
 * An event representing a Customer leaving.
 */
public class LeaveEvent extends SimulatorEvent {
    /**
     * Constructs a new LeaveEvent.
     * @param customer the customer that this event represents.
     * @param serverList the list of servers.
     */
    public LeaveEvent(Customer customer, List<Server> serverList) {
        super(customer, serverList);
    }

    /**
     * Returns a string representation of the object. Shows the event time and customer ID.
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return String.format("%.3f %d leaves", getEventTime(), getCustomerID());
    }

    @Override
    public double getEventTime() {
        return getCustomerArrivalTime();
    }

    /**
     * Execute the event. Since LeaveEvent does not have a next stage, it will return itself.
     * @return itself.
     */
    @Override
    public SimulatorEvent execute() {
        return this;
    }
}