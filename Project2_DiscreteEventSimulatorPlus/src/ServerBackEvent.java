package cs2030.simulator;

/**
 * Represents a server returning from a break.
 */
public class ServerBackEvent extends Event {
    /**
     * Create a new ServerBackEvent object.
     * @param eventTime The time the event occured.
     * @param server The server returning from a break.
     */
    public ServerBackEvent(double eventTime, Server server) {
        super(eventTime, 0.0,
            (shop) -> {
                // Debug Output
                /* System.out.println(
                    String.format("--%s is back at %.3f.--",
                    server.name(), eventTime)); */

                return Pair.of(shop.back(server.identifier()), new EndEvent());
            });
    }
    
    @Override
    public String toString() {
        return "Server Back";
    }
}
