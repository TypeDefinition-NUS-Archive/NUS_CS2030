package cs2030.simulator;

/**
 * Represents a server going for a break.
 */
public class ServerRestEvent extends Event {
    /**
     * Create a new ServerRestEvent object.
     * @param eventTime The time the event occured.
     * @param server The server going for a break.
     */
    public ServerRestEvent(double eventTime, Server server) {
        super(eventTime, 0.0,
            (shop) -> {
                Shop newShop = shop.rest(server.identifier(), eventTime);
                Server newServer = newShop.get(server.identifier());
            
                /* System.out.println(
                    String.format("--%s rests from %.3f until %.3f.--",
                    newServer.name(), eventTime, newServer.nextAvailableTime())); */
            
                return Pair.of(newShop,
                    new ServerBackEvent(newServer.nextAvailableTime(),
                    newServer));
            });
    }
    
    @Override
    public String toString() {
        return "Server Rest";
    }
}
