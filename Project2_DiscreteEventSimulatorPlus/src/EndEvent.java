package cs2030.simulator;

/**
 * Represents the end of a chain of events.
 */
public class EndEvent extends Event {
    /**
     * Create a new EndEvent object.
     */
    public EndEvent() {
        super((shop) -> {
            return Pair.of(shop, new EndEvent());
        });
    }
    
    @Override
    public String toString() {
        return String.format("End Event");
    }
}
