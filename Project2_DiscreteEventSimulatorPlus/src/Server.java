package cs2030.simulator;

/**
 * A server.
 */
public class Server {
    /**
     * The server id.
     */
    private final int identifier;
    /**
     * Server available flag.
     */
    private final boolean isAvailable;
    /**
     * Server next available time.
     */
    private final double nextAvailableTime;
    /**
     * Server customer queue.
     */
    private final CustomerQueue queue;

    /**
     * Create a new Server object.
     * @param identifier The server id.
     * @param isAvailable Server available flag.
     * @param nextAvailableTime Server next available time.
     * @param queue Server customer queue.
     */
    public Server(int identifier, boolean isAvailable,
        double nextAvailableTime, CustomerQueue queue) {
        this.identifier = identifier;
        this.isAvailable = isAvailable;
        this.nextAvailableTime = nextAvailableTime;
        this.queue = queue;
    }

    /**
     * Create a new Server object.
     * @param identifier The server id.
     * @param isAvailable Server available flag.
     * @param hasWaitingCustomer Server has waiting customer flag.
     * @param nextAvailableTime Server next available time.
     */
    public Server(int identifier, boolean isAvailable,
        boolean hasWaitingCustomer, double nextAvailableTime) {
        this.queue = new CustomerQueue(1, hasWaitingCustomer ? 1 : 0);
        this.identifier = identifier;
        this.isAvailable = isAvailable;
        this.nextAvailableTime = nextAvailableTime;
    }

    public final int identifier() {
        return identifier;
    }

    public final boolean isAvailable() {
        return isAvailable;
    }
   
    public final boolean hasWaitingCustomer() {
        return !queue.empty();
    }
   
    public final double nextAvailableTime() {
        return nextAvailableTime;
    }

    public final int queueSize() {
        return queue.size();
    }
    
    public final boolean queueFull() {
        return queue.full();
    }
    
    public final boolean queueAvailable() {
        return !queue.full();
    }
    
    public String name() {
        return String.format("server %d", identifier);
    }
    
    /**
     * Get the queue of the server.
     * @return The queue of the server.
     */
    public final CustomerQueue getQueue() {
        return queue;
    }
    
    /**
     * Replace the queue of the server.
     * @param queue The replacement queue.
     * @return The server with the queue replaced.
     */
    public Server setQueue(CustomerQueue queue) {
        return new Server(identifier, isAvailable, nextAvailableTime, queue);
    }
    
    /**
     * Add one to the queue of the server.
     * @return The server with one added to the queue.
     */
    public Server queue() {
        return new Server(identifier, isAvailable, nextAvailableTime, queue.add());
    }
    
    /**
     * Remove one from the queue of the server.
     * @return The server with one removed from the queue.
     */
    public Server dequeue() {
        return new Server(identifier, isAvailable, nextAvailableTime, queue.remove());
    }
    
    /**
     * Set the state of the server to serving.
     * @param serveStartTime The time the server started serving.
     * @return The server with state set to serving.
     */
    public Server serve(double serveStartTime) {
        return new Server(identifier, false, nextAvailableTime + 1.0, queue.remove());
    }
    
    /**
     * Set the state of the server to done.
     * @return The server with state set to done.
     */
    public Server done() {
        return new Server(identifier, true, nextAvailableTime, queue);
    }
    
    /**
     * Set the state of the server to rest.
     * @return The server with state set to rest.
     */
    public Server rest(double restStartTime) {
        return this;
    }
    
    /**
     * Set the state of the server to back from rest.
     * @return The server with state set to back from rest.
     */
    public Server back() {
        return this;
    }
    
    /**
     * Returns true if the server needs rest. Otherwise, returns false.
     */
    public boolean needRest() {
        return false;
    }

    /**
     * A flag to state if the server shares a queue with other servers.
     */
    public boolean useSharedQueue() {
        return false;
    }

    /**
     * Returns a string representation of the object.
     * @return A string representation of the object.
     */
    @Override
    public String toString() {
        if (isAvailable) {
            return String.format("%d is available", identifier());
        }

        return String.format("%d is busy; %s %.3f",
                identifier(),
                hasWaitingCustomer() ? "waiting customer to be served at" : "available at",
                nextAvailableTime());
    }
}
