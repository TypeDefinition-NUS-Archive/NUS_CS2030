import cs2030.simulator.App;

/**
 * Driver class for the application.
 */
public class Main {
    /**
     * Application main function.
     */
    public static void main(String[] args) {
        App app = new App(args);
        app.run();
    }
}
