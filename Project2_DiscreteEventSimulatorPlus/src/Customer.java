package cs2030.simulator;

import java.util.Optional;

/**
 * A customer.
 */
public class Customer {
    /**
     * ID of the customer.
     */
    private final int id;
    /**
     * Arrival time of the customer.
     */
    private final double arrivalTime;

    /**
     * Create a new Customer object.
     * @param id This is the customer's ID.
     *     It is assigned in chronological order of arrival.
     * @param arrivalTime The time the customer arrives.
     */
    public Customer(int id, double arrivalTime) {
        this.id = id;
        this.arrivalTime = arrivalTime;
    }

    /**
     * Returns a string representation of the object.
     * @return A string representation of the object.
     */
    @Override
    public String toString() {
        return String.format("%d", id());
    }
    
    public String name() {
        return String.format("%d", id());
    }

    public final int id() {
        return id;
    }

    public final double arrivalTime() {
        return arrivalTime;
    }
    
    /**
     * Upon arriving at a shop, choose a
     * server to queue at if none is available.
     * @param shop The shop the customer arrives at.
     */
    public Optional<Server> chooseQueue(Shop shop) {
        return shop.find(Server::queueAvailable);
    }
}
