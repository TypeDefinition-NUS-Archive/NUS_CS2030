package cs2030.simulator;

import java.util.function.Supplier;

/**
 * A human server that has a chance to take a break after serving a customer.
 */
public class HumanServer extends Server {
    /**
     * Supplies the service duration of the server.
     */
    private final Supplier<Double> serviceDuration;
    /**
     * Supplies if the server needs rest.
     */
    private final Supplier<Boolean> needRest;
    /**
     * Supplies the rest duration of the server.
     */
    private final Supplier<Double> restDuration;
    
    /**
     * Create a new HumanServer object.
     * @param identifier The server id.
     * @param isAvailable Server available flag.
     * @param nextAvailableTime Server next available time.
     * @param queue Server customer queue.
     * @param serviceDuration Supplies the service duration of the server.
     * @param needRest Supplies if the server needs rest.
     * @param restDuration Supplies the rest duration of the server.
     */
    public HumanServer(int identifier, boolean isAvailable,
        double nextAvailableTime, CustomerQueue queue,
        Supplier<Double> serviceDuration,
        Supplier<Boolean> needRest,
        Supplier<Double> restDuration) {
        super(identifier, isAvailable, nextAvailableTime, queue);
        this.serviceDuration = serviceDuration;
        this.needRest = needRest;
        this.restDuration = restDuration;
    }
    
    @Override
    public HumanServer setQueue(CustomerQueue queue) {
        return new HumanServer(identifier(), isAvailable(),
            nextAvailableTime(), queue, serviceDuration, needRest, restDuration);
    }
    
    @Override
    public HumanServer queue() {
        return new HumanServer(identifier(), isAvailable(),
            nextAvailableTime(), getQueue().add(), serviceDuration, needRest, restDuration);
    }

    @Override
    public HumanServer dequeue() {
        return new HumanServer(identifier(), isAvailable(),
            nextAvailableTime(), getQueue().remove(), serviceDuration, needRest, restDuration);
    }
    
    @Override
    public HumanServer serve(double serveStartTime) {
        return new HumanServer(identifier(), false,
            serveStartTime + serviceDuration.get(), getQueue().remove(),
            serviceDuration, needRest, restDuration);
    }
    
    @Override
    public HumanServer done() {
        return new HumanServer(identifier(), true, nextAvailableTime(),
            getQueue(), serviceDuration, needRest, restDuration);
    }

    @Override
    public HumanServer rest(double restStartTime) {
        return new HumanServer(identifier(), false,
            restStartTime + restDuration.get(), getQueue(),
            serviceDuration, needRest, restDuration);
    }
    
    @Override
    public HumanServer back() {
        return new HumanServer(identifier(), true, nextAvailableTime(),
            getQueue(), serviceDuration, needRest, restDuration);
    }
    
    @Override
    public boolean needRest() {
        return needRest.get();
    }
}
