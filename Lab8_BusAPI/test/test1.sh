#!/bin/sh

#This MUST be run from the src folder. This script assumes that the file structure is not changed.

mkdir sandbox
cp *.class sandbox/
cp ../test/level1.jar sandbox/
cd sandbox

jar uf level1.jar *.class
echo "16189 Clementi" | java -jar level1.jar

cd ../
rm -r sandbox
