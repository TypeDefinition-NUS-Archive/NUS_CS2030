import java.util.Scanner;
import java.util.ArrayList;
import java.util.NoSuchElementException;

class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numRecords = sc.nextInt();

        Roster roster = new Roster("AY2021");
        for (int i = 0; i < numRecords; i++) {
            String studentKey = sc.next();
            String moduleKey = sc.next();
            String assessmentKey = sc.next();
            String grade = sc.next();
            roster.putIfAbsentAndGet(new Student(studentKey)).
                    flatMap((x) -> { return x.putIfAbsentAndGet(new Module(moduleKey)); }).
                    map((x) -> { return x.putIfAbsentAndGet(new Assessment(assessmentKey, grade)); });
        }

        ArrayList<String> gradeList = new ArrayList<String>();
        try {
            while (sc.hasNextLine()) {
                String studentKey = sc.next();
                String moduleKey = sc.next();
                String assessmentKey = sc.next();
                gradeList.add(roster.getGrade(studentKey, moduleKey, assessmentKey));
            }
        } catch (NoSuchElementException e) {
    	    //break out of while loop; end program
    	} 
        
        for (String str : gradeList) {
            System.out.println(str);
        }
    }
}
