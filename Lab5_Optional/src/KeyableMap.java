import java.util.HashMap;
import java.util.Set;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Iterator;
import java.util.Optional;

abstract class KeyableMap<V extends Keyable> implements Keyable {
    private final String key;
    private final HashMap<String, V> map;

    public KeyableMap(String key) {
        this.key = key;
        this.map = new HashMap<String, V>();
    }

    @Override
    public String toString() {
        String str = String.format("%s: {", key);
        
        Set<Map.Entry<String, V>> entrySet = map.entrySet();
        Iterator<Map.Entry<String, V>> iter = entrySet.iterator();
        while (iter.hasNext()) {
            Map.Entry<String, V> e = iter.next();
            str += e.getValue().toString();
            if (iter.hasNext()) {
                str += ", ";
            }
        }

        str += "}";
        return str;
    }

    @Override
    public String getKey() {
        return key;
    }

    public Optional<V> get(String key) {
        return Optional.ofNullable(map.get(key));
    }

    public <W extends V> KeyableMap<V> put(W value) {
        map.put(value.getKey(), value);
        return this;
    }

    public <W extends V> KeyableMap<V> putIfAbsent(W value) {
        map.putIfAbsent(value.getKey(), value);
        return this;
    }

    public <W extends V> Optional<V> putAndGet(W value) {
        return this.put(value).get(value.getKey());
    }

    public <W extends V> Optional<V> putIfAbsentAndGet(W value) {
        return this.putIfAbsent(value).get(value.getKey());
    }
}
