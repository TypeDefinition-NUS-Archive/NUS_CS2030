Circle createUnitCircleA(Point p, Point q) {
	Point m = p.midPoint(q);
	double theta = p.angleTo(q);
	// Find shortest distance between line pq and circle center c.
	double d = Math.sqrt(1.0 - p.distanceToSquared(m));
	Point c = m.moveTo(theta + (Math.PI * 0.5), d);
	return new Circle(c, 1.0);
}

Circle createUnitCircleB(Point p, Point q) {
	Point m = p.midPoint(q);
	double theta = p.angleTo(q);
	// Find shortest distance between line pq and circle center c.
	double d = Math.sqrt(1.0 - p.distanceToSquared(m));
	Point c = m.moveTo(theta + (Math.PI * 0.5), -d);
	return new Circle(c, 1.0);
}

Circle createUnitCircle(Point p, Point q) {
	return createUnitCircleA(p, q);
}

int findMaxDiscCoverage(Point[] points) {
	int maxCoverage = 0;
	for (int i = 0; i < points.length; ++i) {
		Point p = points[i];
		for (int j = 0; j < points.length; ++j) {
			Point q = points[j];
			if (p == q) { continue; }
			if (p.distanceToSquared(q) > 4.0) { continue; }

			Circle circleA = createUnitCircleA(p, q);
			maxCoverage = Math.max(maxCoverage, circleA.pointsInCircle(points));

			Circle circleB = createUnitCircleB(p, q);
			maxCoverage = Math.max(maxCoverage, circleB.pointsInCircle(points));
		}
	}

	return maxCoverage;
}
