import java.util.function.Function;
import java.util.function.Predicate;
import java.util.Arrays;

class LoggerImpl<T> implements Logger<T> {
    private final T value;
    private final String[] logs;

    protected LoggerImpl(T value, String[] logs) {
        this.value = value;
        this.logs = logs;
    }

    @Override
    public String toString() {
        return String.format("Logger[%s]", value.toString());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof LoggerImpl<?>) {
            LoggerImpl<?> logger = (LoggerImpl<?>)obj;
            return value.equals(logger.value) && Arrays.equals(logs, logger.logs);
        }

        return false;
    }

    @Override
    public void printlog() {
        for (String str : logs) {
            System.out.println(str);
        }
    }

    @Override
    public T getValue() {
        return value;
    }

    @Override
    public String[] getLogs() {
        return logs;
    }

    @Override
    public <U> Logger<U> map(Function<? super T, ? extends U> mapper) {
        // Apply the function.
        U resultValue = (U)(mapper.apply(value));

        // Copy over the logs.
        String[] resultLogs = new String[logs.length + 1];
        System.arraycopy(logs, 0, resultLogs, 0, logs.length);

        // Add new log.
        resultLogs[logs.length] = 
                value.equals(resultValue) ? 
                String.format("Value unchanged. Value = %s", resultValue.toString()) :
                String.format("Value changed! New value = %s", resultValue.toString());
        
        return new LoggerImpl<U>(resultValue, resultLogs);
    }

    @Override
    public <U> Logger<U> flatMap(Function<? super T, ? extends Logger<? extends U>> mapper) {
        Logger<? extends U> resultLogger = (Logger<? extends U>)(mapper.apply(value));
        U resultValue = (U)(resultLogger.getValue());
        String[] resultLogs = resultLogger.getLogs();

        /* This is abso-fucking-lutely disgusting. flatMap should not work this way. The requirements are gross.
        This only works because in the test cases Logger.make(value) is always the first step.
        Should mapper manipulate the given value BEFORE calling Logger.make(value), this will fail.
        It pains me, but based on the requirements, I do not see any other way. */
        String[] combinedLogs = new String[this.logs.length + resultLogs.length - 1]; // Create an array to combine the logs of this Logger and the result.
        System.arraycopy(this.logs, 0, combinedLogs, 0, this.logs.length); // Copy this logger's logs to combinedLogs.
        System.arraycopy(resultLogs, 1, combinedLogs, this.logs.length, resultLogs.length - 1); // Copy resultLog's logs to combinedLogs, but remove the part that says "New Value".

        return new LoggerImpl<U>(resultValue, combinedLogs);
    }

    @Override
    public boolean test(Predicate<? super T> p) {
        return p.test(value);
    }
}
