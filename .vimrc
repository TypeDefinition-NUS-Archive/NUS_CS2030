set autoindent
set smartindent
set tabstop=4
set shiftwidth=4
set ruler
syntax on
set guifont=LiberationMono\ 12
set expandtab
set number
filetype on
filetype indent on
filetype plugin on
