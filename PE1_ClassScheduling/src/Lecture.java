class Lecture extends NUSClass {
    public Lecture(String moduleCode, int classID, String venueID, Instructor instructor, int startTime) {
        super(moduleCode, classID, venueID, instructor, startTime);
    }

    @Override
    public String getIdentifier() {
        return String.format("L%d", getClassID());
    }

    @Override
    public int getDuration() {
        return 2;
    }

    @Override
    public boolean clashWith(NUSClass other) {
        if (!hasOverlappingTimeSlot(other)) {
            return false;
        }
        if (hasSameModule(other)) {
            return true;
        }
        if (hasSameVenue(other)) {
            return true;
        }
        if (hasSameInstructor(other)) {
            return true;
        }
        return false;
    }
}
