interface Constraint {
    public boolean test(Schedule schedule);
}
