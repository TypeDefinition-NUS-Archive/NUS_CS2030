class Cruise {
    private final String identifier;
    private final int arrivalTime;
    private final int numOfLoader;
    private final int serviceTime;

    public Cruise(String _identifier, int _arrivalTime, int _numOfLoader, int _serviceTime) {
        identifier = _identifier;
        arrivalTime = _arrivalTime;
        numOfLoader = _numOfLoader;
        serviceTime = _serviceTime;
    }

    @Override
    public String toString() {
        return identifier + String.format("@%04d", arrivalTime);
    }

    public int getArrivalTime() {
        // Assuming there are no bad inputs such anything above 2359.
        int hour = arrivalTime / 100;
        int minutes = arrivalTime % 100;
        return hour * 60 + minutes;
    }

    public int getNumOfLoadersRequired() {
        return numOfLoader;
    }

    public int getServiceCompletionTime() {
        return serviceTime + getArrivalTime();
    }
}
