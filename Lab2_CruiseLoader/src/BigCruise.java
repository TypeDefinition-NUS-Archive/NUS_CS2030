class BigCruise extends Cruise {
    public BigCruise(String _identifier, int _arrivalTime, int _length, int _passengers) {
        super(_identifier, _arrivalTime, (_length + 39) / 40, (_passengers + 49) / 50);
    }
}
