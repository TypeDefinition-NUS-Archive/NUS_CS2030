import java.util.List;
import java.util.ArrayList;
import java.util.function.Function;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;
import java.util.stream.IntStream;
import java.util.Optional;
import java.util.Scanner;
import java.lang.Integer;

public class Main {
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        List<Integer> intList = new ArrayList<Integer>();
        List<? extends Number> numberList = intList;
        List<Double> doubleList = (List<Double>)numberList;
        
        intList.add(0);
        intList.add(1);
        intList.add(2);
        
        doubleList.add(3.0);
        
        intList.add(4);
        intList.add(5);
        
        System.out.println("This should be a list of Integers:");
        System.out.println(intList);
    }
}
