import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.function.Predicate;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.function.BinaryOperator;

public class LazyList<T> {
    private Supplier<T> head;
    private Supplier<LazyList<T>> tail;
    
    private LazyList() {
        this.head = null;
        this.tail = null;
    }
    
    public LazyList(Supplier<T> head, Supplier<LazyList<T>> tail) {
        this.head = Objects.requireNonNull(head);
        this.tail = Objects.requireNonNull(tail);
    }
    
    public <R> LazyList<R> map(Function<T, R> mapper) {
        return new LazyList<R>(
            () -> mapper.apply(head.get()),
            () -> tail.get().map(mapper));
    }
    
    public void forEach(Consumer<T> consumer) {
        LazyList<T> list = this;
        while (!list.isEmpty()) {
            consumer.accept(list.head.get());
            list = list.tail.get();
        }
    }
    
    public T head() {
        return head.get();
    }
    
    public LazyList<T> tail() {
        return tail.get();
    }
    
    public boolean isEmpty() {
        return this.head == null && this.tail == null;
    }
    
    public static <U> LazyList<U> empty() {
        return new LazyList<U>();
    }
    
    public static <T> LazyList<T> iterate(T init, Predicate<T> cond, UnaryOperator<T> op) {
        if (cond.test(init)) {
            return new LazyList<T>(
                () -> init,
                () -> iterate(op.apply(init), cond, op));
        }
        return LazyList.empty();
    }
   
    public static <U> LazyList<U> concat(LazyList<U> a, LazyList<U> b) {
        if (a.isEmpty()) { return b; }
        return new LazyList<U>(
            () -> a.head(),
            () -> concat(a.tail(), b));
    }
}
