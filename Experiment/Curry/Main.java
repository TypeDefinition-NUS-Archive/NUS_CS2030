import java.util.function.Function;

public class Main {
    public static <S, T, U, R> Function<S, Function<T, Function<U, R>>> curry(TriFunction<S, T, U, R> lambda) {
        Function<S, Function<T, Function<U, R>>> curried = (s) -> (t) -> (u) -> { return lambda.apply(s, t, u); };
        return curried;
    }

    public static void main(String[] args) {
        TriFunction<Integer, Integer, Integer, Integer> foo = (a, b, c) -> { return a + b * c; };
        Function<Integer, Function<Integer, Function<Integer, Integer>>> bar = curry(foo);
        Integer result = bar.apply(5).apply(6).apply(7);
        
        System.out.println("Result is: " + result);
    }
}
