import java.util.Deque;
import java.util.LinkedList;
import java.util.function.Function;
import java.util.NoSuchElementException;
import javax.swing.undo.CannotUndoException;

public class Undoable<T> {
    T value;
    Deque<Object> history;
    
    Undoable(T value, Deque<Object> history) {
        this.value = value;
        this.history = history;
    }
    
    static <U> Undoable<U> of(U value) {
        return new Undoable<U>(value, new LinkedList<Object>());
    }

    public <U> Undoable<U> flatMap(Function<T, Undoable<U>> mapper) {
        // mapper result
        Undoable<U> undoable = mapper.apply(value);
        
        // new value
        U newValue = undoable.value;
        
        // new history
        Deque<Object> newHistory = new LinkedList<Object>(history);
        newHistory.add(value);
        newHistory.addAll(undoable.history);
        
        return new Undoable<U>(newValue, newHistory);
    }
    
    public <U> Undoable<U> map(Function<T, U> mapper) {
        // new value
        U newValue = mapper.apply(value);
        
        // new history
        Deque<Object> newHistory = new LinkedList<Object>(history);
        newHistory.add(value);
        
        return new Undoable<U>(newValue, newHistory);
    }
    
    public T get() {
        return value;
    }
    
    public <U> Undoable<U> undo() {
        Deque<Object> newHistory = new LinkedList<>(history);
        U newValue;

        try {
            newValue = (U)newHistory.removeLast();
        } catch (NoSuchElementException e) {
            throw new CannotUndoException();
        }
        return new Undoable<U>(newValue, newHistory);
    }
}
