import java.util.ArrayList;
import java.util.function.Consumer;

class Logic {
    private ArrayList<Consumer<String>> callbacks;
    
    Logic() {
        callbacks = new ArrayList<Consumer<String>>();
    }

    public void addCallback(Consumer<String> callback) {
        callbacks.add(callback);
    }

    void invoke(String command) {
        // do something based on the command
        // psuedo work being done here.
        
        // call callbacks
        for (Consumer<String> callback : callbacks) {
            callback.accept(command + " executed");
        }
    }
}

