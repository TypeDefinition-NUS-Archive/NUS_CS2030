import java.util.function.Supplier;

public class Recursive<T> extends Compute<T> {
    private final Supplier<Compute<T>> supplier;

    public Recursive(Supplier<Compute<T>> supplier) {
        this.supplier = supplier;
    }
    
    Compute<T> getNext() {
        return supplier.get();
    }
    
    @Override
    T getValue() {
        Compute<T> current = this;
        Compute<T> next = getNext();
        
        while (current != next) {
            current = next;
            next = next.getNext();
        }
        
        return next.getValue();
    }
}
