import java.util.concurrent.RecursiveTask;

public class BinSearch extends RecursiveTask<Boolean> {
    public int low;
    public int high;
    public int toFind;
    public int[] array;
    
    BinSearch(int low, int high, int toFind, int[] array) {
        this.low = low;
        this.high = high;
        this.toFind = toFind;
        this.array = array;
    }
    
    protected Boolean compute() {
        if (high - low <= 1) {
            return array[low] == toFind;
        }
        
        int middle = (low + high) / 2;
        if (array[middle] > toFind) {
            BinSearch left = new BinSearch(low, middle, toFind, array);
            left.fork(); return left.join();
            // return left.compute();
        } else {
            BinSearch right = new BinSearch(middle, high, toFind, array);
            // right.fork(); return right.join();
            return right.compute();
        }
    }
}
