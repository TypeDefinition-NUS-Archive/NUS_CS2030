class Booking {
	private final Driver driver;
	private final Request request;
	private final Service service;
	private final int fare;
	private final int waitTime;

	public Booking(Driver _driver, Request _request) {
		driver = _driver;
		request = _request;
		service = driver.getBestService(_request);
		fare = service.computeFare(_request);
		waitTime = driver.getWaitTime();
	}

	@Override
	public String toString() {
		return String.format("$%.2f", (float)fare * 0.01) + " using " + driver.toString() + " (" + service.toString() + ")";
	}

	public int compareTo(Booking _other) {
		if (fare < _other.fare) {
			return -1;
		}
		if (fare > _other.fare) {
			return 1;
		}
		
		if (waitTime < _other.waitTime) {
			return -1;
		}
		if (waitTime > _other.waitTime) {
			return 1;
		}

		return 0;
	}
}
