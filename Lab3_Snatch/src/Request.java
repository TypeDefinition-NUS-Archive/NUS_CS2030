class Request {
	private final int distance, passengers, time;

	public Request(int _distance, int _passengers, int _time) {
		distance = _distance;
		passengers = _passengers;
		time = _time;
	}

	@Override
	public String toString() {
		return String.format("%dkm for %dpax @ %dhrs", distance, passengers, time);
	}

	public int getDistance() {
		return distance;
	}

	public int getPassengers() {
		return passengers;
	}

	public int getTime() {
		return time;
	}
}
