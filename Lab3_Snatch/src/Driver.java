abstract class Driver {
	private final String license;
	private final int waitTime;

	public Driver(String _license, int _waitTime) {
		license = _license;
		waitTime = _waitTime;
	}

	@Override
	public String toString() {
		return String.format("%s (%d mins away)", license, waitTime);
	}

	public String getLicense() {
		return license;
	}

	public int getWaitTime() {
		return waitTime;
	}

	public abstract Service getBestService(Request _request);
}
