class ShareARide extends Service {
	public ShareARide() {
	}

	@Override
	public String toString() {
		return "ShareARide";
	}

	@Override
	public int computeFare(Request _request) {
		int baseFare = _request.getDistance() * 50;
		int surcharge = (_request.getTime() >= 600 && _request.getTime() <= 900) ? 500 : 0;
		return (baseFare + surcharge)/_request.getPassengers();
    }
}