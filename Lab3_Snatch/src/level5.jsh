Booking findBestBooking(Request _request, Driver[] _drivers) {
    Booking best = new Booking(_drivers[0], _request);

    for (int i = 1; i < _drivers.length; ++i) {
        Booking current = new Booking(_drivers[i], _request);
        if (current.compareTo(best) < 0) {
            best = current;
        }
    }

    return best;
}