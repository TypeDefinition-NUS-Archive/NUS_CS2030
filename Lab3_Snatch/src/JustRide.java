class JustRide extends Service {
	public JustRide() {
	}

	@Override
	public String toString() {
		return "JustRide";
	}

	@Override
	public int computeFare(Request _request) {
		int baseFare = _request.getDistance() * 22;
		int surcharge = (_request.getTime() >= 600 && _request.getTime() <= 900) ? 500 : 0;
		return baseFare + surcharge;
	}
}