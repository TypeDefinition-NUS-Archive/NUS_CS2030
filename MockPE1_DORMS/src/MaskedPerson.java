import java.util.List;
import java.util.ArrayList;

class MaskedPerson extends Person {
    public MaskedPerson(String name, List<Virus> viruses, double shnEndTime) {
        super(name, viruses, shnEndTime);
    }

    public MaskedPerson(String name, List<Virus> viruses) {
        super(name, viruses);
    }

    public MaskedPerson(String name) {
        super(name);
    }

    @Override
    public List<Virus> transmit(double random) {
        if (random > SimulationParameters.MASK_EFFECTIVENESS) {
            return super.transmit(random);
        }

        return new ArrayList<Virus>();
    }

    @Override
    public Person infectWith(List<Virus> listOfViruses, double random) {
        if (random > SimulationParameters.MASK_EFFECTIVENESS) {
            ArrayList<Virus> combinedViruses = new ArrayList<Virus>(getViruses());
            combinedViruses.addAll(listOfViruses);
            return new MaskedPerson(getName(), combinedViruses);
        }

        return new MaskedPerson(getName(), getViruses());
    }

    @Override
    public Person serveSHN(double shnEndTime) {
        return new MaskedPerson(getName(), getViruses(), shnEndTime);
    }
}
