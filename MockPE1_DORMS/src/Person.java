import java.util.List;
import java.util.ArrayList;

class Person {
    private final String name;
    private final ArrayList<Virus> viruses;
    private final double shnEndTime;

    public Person(String name, List<Virus> viruses, double shnEndTime) {
        this.name = name;
        this.viruses = new ArrayList<Virus>(viruses);
        this.shnEndTime = shnEndTime;
    }

    public Person(String name, List<Virus> viruses) {
        this(name, viruses, 0.0);
    }

    public Person(String name) {
        this(name, new ArrayList<Virus>());
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public boolean nameIs(String name) {
        return this.name.equals(name);
    }

    public ArrayList<Virus> getViruses() {
        return viruses;
    }

    public boolean test(String name) {
        for (Virus virus : viruses) {
            if (virus.nameIs(name)) {
                return true;
            }
        }
        return false;
    }

    public List<Virus> transmit(double random) {
        ArrayList<Virus> mutations = new ArrayList<Virus>();
        for (Virus virus : viruses) {
            mutations.add(virus.spread(random));
        }
        return mutations;
    }

    public Person infectWith(List<Virus> listOfViruses, double random) {
        ArrayList<Virus> combinedViruses = new ArrayList<Virus>(viruses);
        combinedViruses.addAll(listOfViruses);
        return new Person(name, combinedViruses);
    }

    public Person serveSHN(double shnEndTime) {
        return new Person(name, viruses, shnEndTime);
    }

    public double getSHNEndTime() {
        return shnEndTime;
    }

    public boolean onSHN(double currentTime) {
        return currentTime < shnEndTime;
    }
}
