abstract class Virus {
    private final String name;
    private final double probabilityOfMutating;

    public Virus(String name, double probabilityOfMutating) {
        this.name = name;
        this.probabilityOfMutating = probabilityOfMutating;
    }

    @Override
    public String toString() {
        return String.format("%s with %.3f probability of mutating", name, probabilityOfMutating);
    }

    public final String getName() {
        return name;
    }

    public final boolean nameIs(String name) {
        return this.name.equals(name);
    }

    public final double getProbabilityOfMutating() {
        return probabilityOfMutating;
    }

    public abstract Virus spread(double random);
}
