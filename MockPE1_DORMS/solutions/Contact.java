import java.util.List;

/**
 * The Contact class marks a human contact between two {@link Person people}
 * and transmits viruses between them
 */
class Contact {

    private final Person first;
    private final Person second;
    private final double time;

    /**
     * Creates a contact between two people and spreads viruses
     * between them
     * @param first  The first person in the contact
     * @param second The second person in the contact
     * @param time   The time this contact was made
     */
    Contact(Person first, Person second, double time) {
        this.first = first;
        this.second = second;
        this.time = time;
    }

    /**
     * Transmits the virus between the two people
     * @param random The random value used to determine the outcome of
     *               diease transmission
     * @return a List of Person objects representing each person after 
     *         disease transmission
     */
    List<Person> transmit(double random) {
        List<Virus> toSecond = this.first.transmit(random);
        List<Virus> toFirst = this.second.transmit(random);
        Person newA = this.first.infectWith(toFirst, random);
        Person newB = this.second.infectWith(toSecond, random);
        return List.of(newA, newB);
    }

    /**
     * Get the people involved in the contact
     * @return The people involved in the contact
     */
    List<Person> getPeople() {
        return List.of(this.first, this.second);
    }

    /**
     * Gets the time of contact
     * @return the time of contact
     */
    double timeOfContact() {
        return this.time;
    }

}

